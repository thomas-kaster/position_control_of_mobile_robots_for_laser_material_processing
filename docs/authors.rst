=======
Credits
=======

Development Lead
----------------

* Thomas Kaster <thomas.kaster@llt.rwth-aachen.de>

Contributors
------------

* Johannes Henseler <johannes.henseler@rwth-aachen.de>
* Leon Gorißen <leon.gorissen@llt.rwth-aachen.de>
* Jan-Niklas Schneider <jan-niklas.schneider@llt.rwth-aachen.de>
* Lucas de Andrade Both <lucas.de.andrade.both@llt.rwth-aachen.de>
* Christian Hinke <christian.hinke@llt.rwth-aachen.de>
