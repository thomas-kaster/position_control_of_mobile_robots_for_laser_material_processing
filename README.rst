Position Control of Mobile Robots for Laser Material Processing
===============================================================

* `Supplementary Information <https://position-control-of-mobile-robots-for-laser-mate-210f8754aa0ace.pages.git-ce.rwth-aachen.de/>`_

Features
--------

* The aim of this paper is to investigate and to compare different position control algorithms that have been designed for and implemented in a mobile robot  
* Four different position control algorithms are compared with each other 
    * P controller
    * PI controller
    * PID controller
    * Lyapunov controller
* These are analyzed on three different paths
    * Circular Path
    * Eight-shaped Path
    * Rectangular Path

Results
-----------------------
* The results are shown for each of the thee different paths

Circular Path
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: resources/Kreis_evaluated2.png

********
Results Circular Path
********

Summary of important numerical values of controller test on circular trajectory

.. list-table:: Results Circular Path
   :header-rows: 1

   - - metric
     - P controller
     - PI controller
     - PID controller
     - Lyapunov controller
   - - mean absolute error in mm
     - 1.45
     - 1.76
     - 0.997
     - 1.196
   - - median absolute error in mm
     - 1.443
     - 1.558
     - 0.908
     - 1.032
   - - share of points above 2 mm target in %
     - 27.866
     - 34.076
     - 9.554
     - 13.535
   - - minimal error in mm
     - 0.029
     - 0.045
     - 0.014
     - 0.012
   - - maximal error in mm
     - 4.19
     - 8.284
     - 4.734
     - 6.43


Eight-shaped Path
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: resources/Infinity_evaluated2.png

********
Results Eight-shaped Path
********

Summary of important numerical values of controller test on eight-shaped trajectory

.. list-table:: Results Eight-shaped Path
   :header-rows: 1

   - - metric
     - P controller
     - PI controller
     - PID controller
     - Lyapunov controller
   - - mean absolute error in mm
     - 2.101
     - 2.791
     - 3.445
     - 3.175
   - - median absolute error in mm
     - 1.966
     - 2.116
     - 2.648
     - 2.306
   - - share of points above 2 mm target in %
     - 49.535
     - 53.753
     - 64.618
     - 58.756
   - - minimal error in mm
     - 0.029
     - 0.014
     - 0.035
     - 0
   - - maximal error in mm
     - 7.237
     - 42.218
     - 39.812
     - 13.155

Rectangular Path
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: resources/Square_evaluated2.png

********
Results Square Path
********

Summary of important numerical values of controller test on square trajectory

.. list-table:: Results Square Path
   :header-rows: 1

   - - metric
     - P controller
     - PI controller
     - PID controller
     - Lyapunov controller
   - - mean absolute error in mm
     - 1.588
     - 1.314
     - 1.781
     - 1.095
   - - median absolute error in mm
     - 1.311
     - 1.172
     - 1.604
     - 0.971
   - - share of points above 2 mm target in %
     - 23.81
     - 13.874
     - 39.348
     - 9.774
   - - minimal error in mm
     - 0.02
     - 0.018
     - 0
     - 0.04
   - - maximal error in mm
     - 14.78
     - 11.321
     - 11.077
     - 8.237

Acknowledgements
--------

* Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germanys Excellence Strategy – EXC-2023 Internet of Production – 390621612
* The authors acknowledge the financial support by the Federal Ministry of Education and Research of Germany in the framework of Research Campus Digital Photonic Production (project number: 13N15423)
* The authors would like to thank Prof. Jeremy Witzens and Dr. Florian Merget from the Institute and Chair of Integrated Photonics at RWTH Aachen University for their support.

.. image:: resources/IoP_Logo.png
.. image:: resources/Logo_Composing_DPP_BMBF_en.png


References
-----------------------
